package prog;

public enum MeanRobotAction {
    WALK,
    HIT_WALL,
    HIT_BY_BULLET,
    SCANNED_ROBOT,
    HIT_ROBOT
}
