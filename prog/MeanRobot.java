package prog;

import java.awt.*;
import java.awt.geom.Point2D;

import robocode.*;
import robocode.util.*;

import java.util.Random;

import static robocode.util.Utils.normalRelativeAngleDegrees;

public class MeanRobot extends AdvancedRobot {

    int hitByBulletCounter = 0;
    static Random random = new Random();
    static double oldEnemyHeading;
    MeanRobotAction action;

    public void run() {

        while (true) {

            setAdjustGunForRobotTurn(true);
            setAdjustRadarForGunTurn(true);
            setAdjustRadarForRobotTurn(true);
            robotMove();
            calculateAction();
            robotMove();

            while (getDistanceRemaining() != 0 || getTurnRemaining() != 0) {
                setTurnRadarRight(360);
                execute();
            }
        }
    }

    public void calculateAction() {
        //  System.out.println("Calculating strategy! :O");
        switch (action) {
            case WALK -> {
                //  System.out.println("WALK in action");
                int randomTurn = (1 + random.nextInt(2));
                setAhead(150 + random.nextInt(200));
                switch (randomTurn) {
                    case 1 -> {
                        setTurnRight(100);
                        // System.out.println("HÖGERSVÄNG!");
                    }
                    case 2 -> {
                        setTurnLeft(100);
                        //  System.out.println("VÄNSTERSVÄNG!");
                    }
                }
                // System.out.println("Walkie walkie :3 " + random);
            }
            case HIT_BY_BULLET -> {
                // System.out.println("OUCH! GOT SHOT :(");
                back(500);
                action = MeanRobotAction.WALK;
            }
            case SCANNED_ROBOT -> {
                //  System.out.println("SCANNED ROBOT WIII");
                action = MeanRobotAction.WALK;
            }
        }
    }

    public void robotMove() {
        //  System.out.println("Robot move");
        if (action == null) {
            action = MeanRobotAction.WALK;
            //Kan lägga kod som gör att robot går i ett mönster
        }
    }

    public void onScannedRobot(ScannedRobotEvent e) {
        int randomColorNumber = (1 + random.nextInt(7));

        switch (randomColorNumber) {
            case 1 -> setAllColors(Color.magenta);
            case 2 -> setAllColors(Color.black);
            case 3 -> setAllColors(Color.cyan);
            case 4 -> setAllColors(Color.green);
            case 5 -> setAllColors(Color.pink);
            case 6 -> setAllColors(Color.white);
            case 7 -> setAllColors(Color.yellow);
        }
        // System.out.println("On scanned robot!!");
        action = MeanRobotAction.SCANNED_ROBOT;
        double bulletPower;
        if (getEnergy() > 30) {
            if (e.getDistance() > 500) {
                bulletPower = 1;
            } else if (e.getDistance() > 300) {
                bulletPower = 2;
            } else {
                bulletPower = 3;
            }
        } else {
            bulletPower = 1;
        }
        double myX = getX();
        double myY = getY();
        double absoluteBearing = getHeadingRadians() + e.getBearingRadians();
        double enemyX = getX() + e.getDistance() * Math.sin(absoluteBearing);
        double enemyY = getY() + e.getDistance() * Math.cos(absoluteBearing);
        double enemyHeading = e.getHeadingRadians();
        double enemyHeadingChange = enemyHeading - oldEnemyHeading;
        double enemyVelocity = e.getVelocity();
        oldEnemyHeading = enemyHeading;

        double deltaTime = 0;
        double battleFieldHeight = getBattleFieldHeight(),
                battleFieldWidth = getBattleFieldWidth();
        double predictedX = enemyX, predictedY = enemyY;
        while ((++deltaTime) * (20.0 - 3.0 * bulletPower) <
                Point2D.Double.distance(myX, myY, predictedX, predictedY)) {
            predictedX += Math.sin(enemyHeading) * enemyVelocity;
            predictedY += Math.cos(enemyHeading) * enemyVelocity;
            enemyHeading += enemyHeadingChange;
            if (predictedX < 18.0
                    || predictedY < 18.0
                    || predictedX > battleFieldWidth - 18.0
                    || predictedY > battleFieldHeight - 18.0) {

                predictedX = Math.min(Math.max(18.0, predictedX),
                        battleFieldWidth - 18.0);
                predictedY = Math.min(Math.max(18.0, predictedY),
                        battleFieldHeight - 18.0);
                break;
            }
        }
        double theta = Utils.normalAbsoluteAngle(Math.atan2(
                predictedX - getX(), predictedY - getY()));

        setTurnRadarRightRadians(Utils.normalRelativeAngle(
                absoluteBearing - getRadarHeadingRadians()));
        setTurnGunRightRadians(Utils.normalRelativeAngle(
                theta - getGunHeadingRadians()));

        fire(bulletPower);

        if (e.getEnergy() < 30) {
            turnRight(e.getBearing());
            if (getEnergy() > 40 && e.getEnergy() < 10) {
                // System.out.println("RAMMIIIING!!!" + e.getName());
                setAhead(e.getDistance());
            } else if (e.getDistance() > 200) {
                if (e.getBearing() == getHeading() && e.getVelocity() > 0) {
                    //   System.out.println("DRAAAAAR!!!");
                    setBack(10);
                    turnRight(15);
                    action = MeanRobotAction.WALK;
                } else {
                    //  System.out.println("FOLLOWIIING!!!");
                    setAhead(e.getDistance() - 20);
                }
            }
        } else {
            action = MeanRobotAction.WALK;
        }
    }

    public void onHitByBullet(HitByBulletEvent e) {
        hitByBulletCounter++;
      //  System.out.println("On hit by bullet" + hitByBulletCounter);
        action = MeanRobotAction.HIT_BY_BULLET;

        if (hitByBulletCounter < 3) {
            if (getEnergy() < 30) {
                if (e.getBearing() > -90 && e.getBearing() < 90) {   // Framifrån
                    setAhead(-40);
                    action = MeanRobotAction.WALK;
                } else {   // Bakifrån
                    setAhead(30);
                    action = MeanRobotAction.WALK;
                }
            }
            if (getEnergy() > 30) {
                if (e.getBearing() > -90 && e.getBearing() < 90) { // Framifrån
                    if (e.getBearing() > -90 && e.getBearing() < 0) {  // Vänster fram
                        turnRight(normalRelativeAngleDegrees(90 - (getHeading() - e.getHeading())));
                    } else {  // Höger Fram
                        turnLeft(normalRelativeAngleDegrees(90 - (getHeading() - e.getHeading())));
                    }
                    setAhead(-85);
                    setTurnRadarRight(e.getBearing());
                    action = MeanRobotAction.WALK;
                } else {  // Bakifrån
                    if (e.getBearing() > -180 && e.getBearing() < -90) {  // Vänster bak
                        turnLeft(normalRelativeAngleDegrees(90 - (getHeading() - e.getHeading())));
                    } else {  // Höger Bak
                        turnRight(normalRelativeAngleDegrees(90 - (getHeading() - e.getHeading())));
                    }
                    setTurnRadarRight(e.getBearing());
                    action = MeanRobotAction.WALK;
                }
            }
        } else {
           // System.out.println("FLYR VI ELLER?!");
            turnRight(e.getBearing() + 90);
            setAhead(60);
            hitByBulletCounter = 0;
        }
    }

    public void onHitRobot(HitRobotEvent e) {
      //  System.out.println("On hit Robot");
        if (e.getBearing() > -90 && e.getBearing() < 90) {
            setAhead(-65);
            setTurnRight(e.getBearing() + 90);
            action = MeanRobotAction.WALK;
        } else {
            setAhead(65);
            setTurnRight(e.getBearing() + 90);
            action = MeanRobotAction.WALK;
        }
    }

    public void onHitWall(HitWallEvent e) {
      //  System.out.println("This is the onHitWall method :O");

        if (e.getBearing() > -90 && e.getBearing() < 90) {
            if (e.getBearing() > -90 && e.getBearing() < 0) {
                setTurnRight(e.getBearing() - e.getBearing() + 100);
                action = MeanRobotAction.WALK;
            } else {
                setTurnLeft(e.getBearing() - e.getBearing() + 100);
                action = MeanRobotAction.WALK;
            }
        } else {
            if (e.getBearing() < 180 && e.getBearing() > 90) {
                setTurnLeft(e.getBearing() - e.getBearing() / 2);
                action = MeanRobotAction.WALK;

            } else {
                setTurnRight(50);
                action = MeanRobotAction.WALK;
            }
        }
    }
}
